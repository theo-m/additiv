import React from 'react';
import {StatusBar, SafeAreaView, StyleSheet} from 'react-native';
import Explorer from './app/screens/explorer/explorer';

const App = () => {
  return (
    <SafeAreaView style={style.flexOne}>
      <StatusBar backgroundColor="#004c40" />
      <Explorer />
    </SafeAreaView>
  );
};

const style = StyleSheet.create({
  flexOne: {flex: 1},
});

export default App;
