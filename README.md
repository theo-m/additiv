# Additiv Employee Explorer

Employee Explorer app: a directory application which allows the user to expore the company structure. A user of the application is able to see the names and team members of every supervisor.

## Installation

### `git clone git@bitbucket.org:theo-m/additiv.git`

Clone the repository.

### `yarn install`

Initialize the project.

### `cd ios`

### `pod install`

iOS setup instructions.

## Available Scripts

In the project directory, you can run:

### `yarn ios`

Run the iOS app on a connected emulator.

### `yarn test`

Launch the test runner.
