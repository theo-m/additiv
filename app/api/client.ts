import {ApisauceInstance, create} from 'apisauce';

const URL = 'https://api.additivasia.io/api/v1';

// Client used to fetch the employee details.
const apiClient: ApisauceInstance = create({
  baseURL: URL,
});

export default apiClient;
