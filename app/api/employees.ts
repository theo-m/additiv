import {ApiResponse} from 'apisauce';
import {EmployeeResponse} from '../interfaces/employee-response';
import client from './client';

const endpoint = '/assignment/employees/';

// Endpoint that contains the employee details used to create the subordinate tree.
const getEmployee = async (
  name: string,
): Promise<ApiResponse<EmployeeResponse, unknown>> =>
  await client.get(`${endpoint}${name}`);

export default {
  getEmployee,
};
