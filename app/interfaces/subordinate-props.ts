//** Interface Definition for the Subordinate Component */
export interface SubordinateProps {
  subordinate: string;
  idx: number;
  isDirect: boolean;
  jobTitle: string;
}
