import {EmployeeMap} from '../types/employee-map';

//** Interface Definition for the Directory Component */
export interface DirectoryProps {
  keyName: string;
  employeeName: string;
  employeeMap: EmployeeMap;
  searchQuery: string;
}
