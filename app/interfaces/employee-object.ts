//** Interface Definition for the Employee Object */
export interface EmployeeObject {
  name: string;
  details: {
    jobTitle: string;
    isSupervisor: boolean;
    directSubordinates: [string];
  };
}
