//** Interface Definition for the Employee Response Endpoint */
export interface EmployeeResponse {
  [0]: string;
  [1]: {
    'direct-subordinates': string[];
  };
}
