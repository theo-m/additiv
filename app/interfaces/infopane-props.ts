//** Interface Definition for the Infopane Component */
export interface InfopaneProps {
  employeeMap: Map<
    string,
    {
      jobTitle: string;
      isSupervisor: boolean;
      directSubordinates: [string];
    }
  >;
  searchQuery: string;
  countIndirectSubordinates: number;
}
