import {EmployeeObject} from '../interfaces/employee-object';

export const employeeDirectoryMethods = () => {
  /**
   * @description Filter logic to skip subordinate names which are already part of the structured tree.
   * @memberof `employeeDirectoryMethods`
   */
  const filterOnUniqueNames = (
    childName: string,
    uniqueSubordinatesList: string[],
  ): boolean => {
    const isNameNew = !uniqueSubordinatesList.includes(childName);
    if (isNameNew) {
      uniqueSubordinatesList.push(childName);
    }

    return isNameNew;
  };

  /**
   * @description Used when a server response with raw employee details is received. Transforms raw employee details to an Employee object.
   * @memberof `employeeDirectoryMethods`
   */
  const returnEmployee = (
    nameInput: string,
    employeeObjectInput: any,
    uniqueSubordinatesList: string[],
  ): EmployeeObject => {
    const [jobTitle, potentialSubordinates] = employeeObjectInput;
    const hasSubordinates = potentialSubordinates
      ? 'direct-subordinates' in potentialSubordinates
      : false;
    const directSubordinates = hasSubordinates
      ? potentialSubordinates['direct-subordinates']
      : [];

    const filteredSubordinates = directSubordinates.filter(
      (subordinateName: string): boolean =>
        filterOnUniqueNames(subordinateName, uniqueSubordinatesList),
    );
    const isSupervisor = jobTitle.toLowerCase().includes('supervisor');
    return {
      name: nameInput,
      details: {
        jobTitle,
        isSupervisor,
        directSubordinates: filteredSubordinates,
      },
    };
  };

  /**
   * @description Used to query subordinate details when processing the server response of the parent employee.
   * This step consists of the recursive call to fetch new employee details which constructs the structured tree of all subordinates.
   * @memberof `employeeDirectoryMethods`
   */
  const traverseSubordinateNames = (
    employeeProcessed: EmployeeObject,
    queryEndpoint: (subordinateName: string) => Promise<void>,
  ): void => {
    // Step 3: traverse subordinates if present.
    employeeProcessed.details?.directSubordinates?.forEach(
      (subordinateName: string): void => {
        queryEndpoint(subordinateName);
      },
    );
  };

  /**
   * @description Used to set the backgroundcolor of the subordinate cards.
   * @memberof `employeeDirectoryMethods`
   */
  const returnColorPalette = (isDirect: boolean, isEven: boolean) => {
    if (isDirect && isEven) {
      return '#9e9efd';
    } else if (isDirect && !isEven) {
      return '#7f7fff';
    } else if (!isDirect && isEven) {
      return '#9e9efd81';
    } else if (!isDirect && !isEven) {
      return '#7f7fff88';
    }
  };

  return {
    filterOnUniqueNames,
    returnEmployee,
    traverseSubordinateNames,
    returnColorPalette,
  };
};
