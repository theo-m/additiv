import {employeeDirectoryMethods} from './employee-directory-methods';

describe('Employee directory methods', () => {
  test('filterOnUniqueNames, case name is new', () => {
    const actual = employeeDirectoryMethods().filterOnUniqueNames('tom', [
      'lou',
      'harry',
    ]);
    expect(actual).toBeTruthy();
  });

  test('filterOnUniqueNames, case name already exists', () => {
    const actual = employeeDirectoryMethods().filterOnUniqueNames('tom', [
      'tom',
      'harry',
    ]);
    expect(actual).toBeFalsy();
  });

  test('returnEmployee, case is employee without subordinates', () => {
    const actual = employeeDirectoryMethods().returnEmployee(
      'Tom',
      ['Employee'],
      [],
    );
    expect(actual).toEqual({
      details: {
        directSubordinates: [],
        isSupervisor: false,
        jobTitle: 'Employee',
      },
      name: 'Tom',
    });
  });

  test('returnEmployee, case is supervisor with all new and unique subordinates', () => {
    const actual = employeeDirectoryMethods().returnEmployee(
      'Tom',
      ['Supervisor', {'direct-subordinates': ['Samad Pitt', 'James Webb']}],
      ['Sam'],
    );
    expect(actual).toEqual({
      details: {
        directSubordinates: ['Samad Pitt', 'James Webb'],
        isSupervisor: true,
        jobTitle: 'Supervisor',
      },
      name: 'Tom',
    });
  });

  test('returnEmployee, case is supervisor with some new and unique subordinates', () => {
    const actual = employeeDirectoryMethods().returnEmployee(
      'Tom',
      ['Supervisor', {'direct-subordinates': ['Samad Pitt', 'James Webb']}],
      ['Samad Pitt'],
    );
    expect(actual).toEqual({
      details: {
        directSubordinates: ['James Webb'],
        isSupervisor: true,
        jobTitle: 'Supervisor',
      },
      name: 'Tom',
    });
  });

  test('returnEmployee, case is supervisor with some no new and unique subordinates', () => {
    const actual = employeeDirectoryMethods().returnEmployee(
      'Tom',
      ['Supervisor', {'direct-subordinates': ['Samad Pitt', 'James Webb']}],
      ['Samad Pitt', 'James Webb'],
    );
    expect(actual).toEqual({
      details: {
        directSubordinates: [],
        isSupervisor: true,
        jobTitle: 'Supervisor',
      },
      name: 'Tom',
    });
  });

  test('returnColorPalette, case item is direct subordinate and even row', () => {
    const actual = employeeDirectoryMethods().returnColorPalette(true, true);
    expect(actual).toEqual('#9e9efd');
  });
  test('returnColorPalette, case item is direct subordinate and uneven row', () => {
    const actual = employeeDirectoryMethods().returnColorPalette(true, false);
    expect(actual).toEqual('#7f7fff');
  });
  test('returnColorPalette, case item is direct indirect subordinate and even row', () => {
    const actual = employeeDirectoryMethods().returnColorPalette(false, true);
    expect(actual).toEqual('#9e9efd81');
  });
  test('returnColorPalette, case item is direct indirect subordinate and uneven row', () => {
    const actual = employeeDirectoryMethods().returnColorPalette(false, false);
    expect(actual).toEqual('#7f7fff88');
  });
});
