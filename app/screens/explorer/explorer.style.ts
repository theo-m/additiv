import styled from 'styled-components/native';

export const ExplorerContainer = styled.View`
  flex: 1;
  background-color: #e9e9e9e6;
  align-items: center;
  justify-content: flex-start;
  padding-top: 20px;
`;

export const HeaderText = styled.Text`
  padding: 10px;
  font-size: 20px;
`;

export const SpacerVertical = styled.View`
  padding-top: 3px;
  padding-bottom: 3px;
`;

export const PaddedScrollView = styled.ScrollView`
  width: 95%;
`;
