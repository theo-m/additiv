import React from 'react';
import {useEffect, useState} from 'react';
import additivAsia from '../../api/employees';
import {EmployeeObject} from '../../interfaces/employee-object';
import {employeeDirectoryMethods} from '../../utils/employee-directory-methods';
import SearchBar from 'react-native-dynamic-search-bar';
import Directory from '../../components/directory/directory';
import Infopane from '../../components/infopane/infopane';
import {
  ExplorerContainer,
  HeaderText,
  PaddedScrollView,
  SpacerVertical,
} from './explorer.style';
import {EmployeeMap} from '../../types/employee-map';

/**
 * Employee Explorer screen. This is the main view of the directory application and allows the user
 * to expore the company structure. A user of the application is able to see the names and team members of every supervisor.
 */
const Explorer = () => {
  const [employeeMap, setEmployeeMap] = useState<EmployeeMap>(new Map());
  const [queryResult, setQueryResult] = useState<EmployeeObject | null>();
  const [searchQuery, setSearchQuery] = useState<string>('');
  const [supervisorList, setSupervisorList] = useState<string[]>([]);
  const [countIndirectSubordinates, setCountIndirectSubordinates] =
    useState<number>(0);
  const [isLoading, setIsLoading] = useState<boolean>(false);

  /**
   * useEffect used to process the employee object returned for a matched employee name:
   * - Adds the employee object to the employee map.
   * - If the employee is a supervisor adds the employee name to the supervisorList.
   * - Updates the count of indirect subordinates for the employee name.
   */
  useEffect((): void => {
    if (queryResult) {
      const {name, details} = queryResult;
      if (employeeMap.get(name) === undefined) {
        // Add employee to the employeeMap.
        setEmployeeMap(prev => new Map([...prev, [name, details]]));
        // Update the list of all supervisors.
        if (details.isSupervisor && name !== searchQuery) {
          setSupervisorList(prev => [...prev, name]);
        }
        // Update indirect suboordinate count displayed in the search results.
        const countDirectSubordinates =
          employeeMap.get(searchQuery)?.directSubordinates.length || 0;
        setCountIndirectSubordinates(
          employeeMap.size - countDirectSubordinates,
        );
        setQueryResult(null);
      }
    }
  }, [queryResult, employeeMap, searchQuery]);

  // useEffect used to trigger the generateEmployeeTree when the user submits a search query.
  useEffect((): void => {
    /**
     * `generateEmployeeTree` is used to generate the subordinate tree:
     * 1. Calls the search endpoint with the `employeeName` search query.
     * 2. In case of a match transforms the server response to a JSON object with the `returnEmployee` method.
     * 3. If the matched employee has subordinates will fetch the employee details for all subordinate names by calling step 1. again (recursive step).
     * 4. If no match will set the screen to indicate no match found.
     */
    async function generateEmployeeTree(employeeName: string) {
      // This variable is used to track subordinate names to meet the assignment requirement of only showing unique subordinate names.
      let childNames: string[] = [];

      async function searchEmployeeName(employeeNameInput: string) {
        const queryResponse = await additivAsia.getEmployee(employeeNameInput);
        setIsLoading(true);
        if (queryResponse.ok) {
          setIsLoading(false);
          // Step 2: if match: add transforms response and add employee object to employee map.
          const employeeObject = employeeDirectoryMethods().returnEmployee(
            employeeNameInput,
            queryResponse.data,
            childNames,
          );
          setQueryResult(employeeObject);
          // Step 3: traverse subordinates if present.
          employeeDirectoryMethods().traverseSubordinateNames(
            employeeObject,
            searchEmployeeName,
          );
        } else {
          // Sets the screen to say no match found.
          clearSearchResults();
          setIsLoading(false);
        }
      }
      return searchEmployeeName(employeeName);
    }
    // Step 1: query endpoint.
    if (searchQuery) {
      clearSearchResults();
      generateEmployeeTree(searchQuery);
    }
  }, [searchQuery]);

  /** Clear current search results. Used when the user presses the `X` on the search bar
   * or when the user submits a search query.
   */
  const clearSearchResults = () => {
    setEmployeeMap(new Map());
    setSupervisorList([]);
  };

  const hasEmployees = employeeMap.size > 0;
  const hasSupervisors = supervisorList.length > 0;
  const hasSearchQuery = searchQuery !== '';
  const indirectSubordinates = supervisorList.map((supervisor, idx) => (
    <Directory
      key={`${supervisor}${idx}`}
      keyName={`${supervisor}${idx}`}
      employeeName={supervisor}
      employeeMap={employeeMap}
      searchQuery={searchQuery}
    />
  ));

  return (
    <ExplorerContainer>
      <HeaderText>Employee Explorer</HeaderText>
      <SearchBar
        autoCorrect={false}
        keyboardType="web-search"
        onClearPress={() => {
          clearSearchResults();
          setSearchQuery('');
        }}
        onSubmitEditing={({nativeEvent}) => {
          clearSearchResults();
          setSearchQuery(nativeEvent.text.trim());
        }}
        placeholder="Search here"
        spinnerVisibility={isLoading}
      />
      <SpacerVertical />
      <Infopane
        searchQuery={searchQuery}
        employeeMap={employeeMap}
        countIndirectSubordinates={countIndirectSubordinates}
      />
      <SpacerVertical />
      <PaddedScrollView>
        {hasEmployees && hasSearchQuery && (
          <Directory
            keyName={`${searchQuery}direct`}
            employeeName={searchQuery}
            employeeMap={employeeMap}
            searchQuery={searchQuery}
          />
        )}
        {hasSupervisors && indirectSubordinates}
      </PaddedScrollView>
    </ExplorerContainer>
  );
};

export default Explorer;
