//** Type Definition for the Employee Map */
export type EmployeeMap = Map<
  string,
  {
    jobTitle: string;
    isSupervisor: boolean;
    directSubordinates: [string];
  }
>;
