import React from 'react';
import {render} from '@testing-library/react-native';

import Subordinate from './subordinate';

describe('UI components: Subordinate', () => {
  test('case single subordinate: displays employee name', () => {
    const {getByText} = render(
      <Subordinate
        key={'subordinate'}
        idx={1}
        subordinate={'Tom Sawyer'}
        jobTitle={'Janitor'}
        isDirect={true}
      />,
    );
    const fullNameText = getByText('Tom Sawyer');
    expect(fullNameText).not.toBeNull();
  });

  test('case single subordinate: displays job title', () => {
    const {getByText} = render(
      <Subordinate
        key={'subordinate'}
        idx={1}
        subordinate={'Tom Sawyer'}
        jobTitle={'Janitor'}
        isDirect={true}
      />,
    );
    const jobTitleText = getByText('Janitor');
    expect(jobTitleText).not.toBeNull();
  });
});
