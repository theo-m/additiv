import React from 'react';
import {SubordinateProps} from '../../interfaces/subordinate-props';
import {
  SubordinateContainer,
  SubordinateName,
  SubordinateJobTitle,
} from './subordinate.style';

/**
 * @description Subordinate Component used in the search results view to display a subordinate details.
 * @memberof `ui.components`
 */
const Subordinate = ({
  subordinate,
  idx,
  isDirect,
  jobTitle,
}: SubordinateProps): JSX.Element => {
  return (
    <SubordinateContainer
      isEven={idx % 2 === 0}
      isDirect={isDirect}
      key={`${subordinate}${idx}`}>
      <SubordinateName>{subordinate}</SubordinateName>
      <SubordinateJobTitle>{jobTitle}</SubordinateJobTitle>
    </SubordinateContainer>
  );
};

export default Subordinate;
