import styled from 'styled-components/native';
import {employeeDirectoryMethods} from '../../utils/employee-directory-methods';

export const SubordinateContainer = styled.View`
  background-color: ${({
    isEven,
    isDirect,
  }: {
    isEven: boolean;
    isDirect: boolean;
  }) => {
    if (isEven) {
      return employeeDirectoryMethods().returnColorPalette(isDirect, true);
    }
    return employeeDirectoryMethods().returnColorPalette(isDirect, false);
  }};
  border-radius: 3px;
  padding: 5px;
  margin-top: 5px;
  justify-content: center;
  flex-direction: column;
`;

export const SubordinateName = styled.Text`
  letter-spacing: 1px;
  font-size: 18px;
  color: #000;
`;

export const SubordinateJobTitle = styled.Text`
  letter-spacing: 1px;
  font-size: 14px;
  color: #2c2c2c;
`;
