import React from 'react';
import {InfopaneProps} from '../../interfaces/infopane-props';
import {
  InstructionsText,
  NoMatchText,
  SearchResultsText,
} from './infopane.style';

/**
 * @description Infopane Component displayed below the search bar used to provide updates on the search query results.
 * @memberof `ui.components`
 */
const Infopane = ({
  searchQuery,
  employeeMap,
  countIndirectSubordinates,
}: InfopaneProps): JSX.Element | null => {
  // Search term without structured tree present, no match.
  if (employeeMap.size === 0 && searchQuery !== '') {
    return (
      <>
        <NoMatchText>
          Unable to find employee name, please try again.
        </NoMatchText>
      </>
    );
    // Empty search term and empty structured tree, no search started.
  } else if (employeeMap.size === 0 && searchQuery === '') {
    return (
      <>
        <InstructionsText>
          Instructions: Type an employee name to display any direct and indirect
          subordinates related to that employee.
        </InstructionsText>
      </>
    );
  } else if (
    employeeMap.size > 0 &&
    employeeMap.get(searchQuery) &&
    employeeMap.get(searchQuery)?.directSubordinates?.length > 0 &&
    searchQuery !== ''
  ) {
    const directSubordinatesCount =
      employeeMap.get(searchQuery)?.directSubordinates.length;
    return (
      <SearchResultsText>
        Found {directSubordinatesCount} direct subordinates and{' '}
        {countIndirectSubordinates} indirect subordinates.
      </SearchResultsText>
    );
  } else {
    return null;
  }
};

export default Infopane;
