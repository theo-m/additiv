import React from 'react';
import {render} from '@testing-library/react-native';

import Infopane from './infopane';

describe('UI components: Infopane', () => {
  test('case 1 direct subordinate 0 indirect subordinates: display match counts', () => {
    const countIndirectSubordinates = 0;
    const searchQuery = 'John Doe';
    let employeeMap = new Map();
    employeeMap.set('John Doe', {
      jobTitle: 'CEO',
      isSupervisor: false,
      directSubordinates: ['Tom Sawyer'],
    });

    const {getByText} = render(
      <Infopane
        searchQuery={searchQuery}
        employeeMap={employeeMap}
        countIndirectSubordinates={countIndirectSubordinates}
      />,
    );
    const infoPaneText = getByText(
      'Found 1 direct subordinates and 0 indirect subordinates.',
    );
    expect(infoPaneText).not.toBeNull();
  });

  test('case empty search query: display search instructions', () => {
    const countIndirectSubordinates = 0;
    const searchQuery = '';
    let employeeMap = new Map();

    const {getByText} = render(
      <Infopane
        searchQuery={searchQuery}
        employeeMap={employeeMap}
        countIndirectSubordinates={countIndirectSubordinates}
      />,
    );
    const infoPaneText = getByText(
      'Instructions: Type an employee name to display any direct and indirect subordinates related to that employee.',
    );
    expect(infoPaneText).not.toBeNull();
  });

  test('case query with no match: display no match found', () => {
    const countIndirectSubordinates = 0;
    const searchQuery = 'the invisible employee';
    let employeeMap = new Map();

    const {getByText} = render(
      <Infopane
        searchQuery={searchQuery}
        employeeMap={employeeMap}
        countIndirectSubordinates={countIndirectSubordinates}
      />,
    );
    const infoPaneText = getByText(
      'Unable to find employee name, please try again.',
    );
    expect(infoPaneText).not.toBeNull();
  });
});
