import styled from 'styled-components/native';

export const NoMatchText = styled.Text`
  padding: 10px;
  padding-left: 30px;
  padding-right: 30px;
  font-size: 14px;
  color: tomato;
`;

export const InstructionsText = styled.Text`
  padding: 10px;
  padding-left: 30px;
  padding-right: 30px;
  font-size: 14px;
`;

export const SearchResultsText = styled.Text`
  padding: 10px;
  padding-left: 30px;
  padding-right: 30px;
  font-size: 14px;
`;
