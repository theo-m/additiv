import styled from 'styled-components/native';

export const DirectoryContainer = styled.View`
  flex: 1;
`;

export const DirectoryHeader = styled.View`
  background-color: #fff;
`;

export const DirectoryHeaderText = styled.Text`
  padding: 10px;
  font-size: 17px;
  font-weight: bold;
`;

export const DirectorySeparator = styled.View`
  flex: 1;
  border-bottom-color: #c3c3c3;
  border-bottom-width: 1px;
  margin-top: 10px;
  margin-bottom: 10px;
`;
