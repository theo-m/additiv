import React from 'react';
import {render} from '@testing-library/react-native';

import Directory from './directory';

describe('UI components: Directory', () => {
  test('case valid employee with one subordinate: displays main employee name and subordinate count', () => {
    const searchQuery = 'John Doe';
    let employeeMap = new Map();
    employeeMap.set('John Doe', {
      jobTitle: 'CEO',
      isSupervisor: false,
      directSubordinates: ['Tom Sawyer'],
    });
    employeeMap.set('Tom Sawyer', {
      jobTitle: 'employee',
      isSupervisor: false,
      directSubordinates: [],
    });

    const {getByText} = render(
      <Directory
        keyName={`directory${new Date()}`}
        employeeName={'John Doe'}
        employeeMap={employeeMap}
        searchQuery={searchQuery}
      />,
    );
    const fullNameText = getByText('Direct Subordinates of CEO: John Doe (1)');
    expect(fullNameText).not.toBeNull();
  });

  test('case valid employee without any subordinates: displays main employee name and no subordinate message', () => {
    const searchQuery = 'John Doe';
    let employeeMap = new Map();
    employeeMap.set('John Doe', {
      jobTitle: 'CEO',
      isSupervisor: false,
      directSubordinates: [],
    });

    const {getByText} = render(
      <Directory
        keyName={`directory${new Date()}`}
        employeeName={'John Doe'}
        employeeMap={employeeMap}
        searchQuery={searchQuery}
      />,
    );
    const fullNameText = getByText(
      'John Doe does not have any direct subordinates.',
    );
    expect(fullNameText).not.toBeNull();
  });
});
