import React from 'react';
import {DirectoryProps} from '../../interfaces/directory-props';
import Subordinate from '../subordinate/subordinate';
import {
  DirectoryContainer,
  DirectoryHeader,
  DirectoryHeaderText,
  DirectorySeparator,
} from './directory.style';

/**
 * @description Directory Component used in the search results view to display a grouped listing of subordinates.
 * @memberof `ui.components`
 */
const Directory = ({
  keyName,
  employeeName,
  employeeMap,
  searchQuery,
}: DirectoryProps): JSX.Element => {
  const employeeJobTitle = employeeMap.get(employeeName)?.jobTitle || '';
  const subordinateList =
    employeeMap.get(employeeName)?.directSubordinates || [];
  const subordinateCount = subordinateList.length;

  if (subordinateCount === 0) {
    return (
      <DirectoryHeaderText>
        {employeeName} does not have any direct subordinates.
      </DirectoryHeaderText>
    );
  }

  const hasSubordinates = subordinateList.length > 0;
  const subordinateListItems = subordinateList.map((subordinate, idx) => (
    <Subordinate
      key={`subordinate${keyName}${idx}`}
      idx={idx}
      subordinate={subordinate}
      jobTitle={employeeMap.get(subordinate)?.jobTitle || ''}
      isDirect={employeeName === searchQuery}
    />
  ));

  return (
    <DirectoryContainer key={`directory${keyName}`}>
      <DirectoryHeader>
        <DirectoryHeaderText>
          Direct Subordinates of {employeeJobTitle}: {employeeName} (
          {subordinateCount})
        </DirectoryHeaderText>
      </DirectoryHeader>

      {hasSubordinates && subordinateListItems}
      <DirectorySeparator />
    </DirectoryContainer>
  );
};

export default Directory;
